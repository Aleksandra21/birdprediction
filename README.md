# Machine learning(ML): Bird exploration and classification
Measurements of bones and ecological groups of birds


# About the project
In this project we will perform exploratory data analysis (EDA) on the dataset,such as identifying outliers, correlated 
attributes and creating visualizations that will help to understand the data.
The final purpose of the project is to classify birds into their ecological groups based on their bone measurements 
using machine learning.


Ecological groups an their labels:

*    SW: Swimming Birds 
*    W:  Wading Birds
*    T:  Terrestrial Birds
*    R:  Raptors
*    P:  Scansorial Birds
*    SO: Singing Birds


# About the dataset
The dataset contains 420 birds. Each bird is represented by 10 measurements. 

*    Length and Diameter of Humerus
*    Length and Diameter of Ulna
*    Length and Diameter of Femur
*    Length and Diameter of Tibiotarsus
*    Length and Diameter of Tarsometatarsus

